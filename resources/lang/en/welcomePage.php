<?php

return [
    'bannerTop' => 'Give a Hand',
    'bannerMid' => 'To make the world a better place!',
    'bannerBot' => 'People need your help,so why don\'t you be the one who draws a smile on their faces.',

    'causesTitle' => 'Our main goals',
    'cause1Title' => 'Help People',
    'cause1Body' => 'We want to help people. Every service we offer is in favour of those in need.',
    'cause2Title' => 'Give Inspiration',
    'cause2Body' => 'We want to be an example for other associations and communities.',
    'cause3Title' => 'Help the needy people',
    'cause3Body' => 'There are some people who can\'t afford even the basic necessities, and we decide to help those by collecting money for them.',

    'noProfit' => "We are a nonprofit team <br>
                   that works currently in <span style='color: red'>Tunisia</span>
                   with plans to expand more in the future.",
    'noProfitSub' => "Our goal will <u>Never</u> be profitable."
];
