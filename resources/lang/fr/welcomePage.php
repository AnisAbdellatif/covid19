<?php

return [
    'bannerTop' => 'Donnez un coup de main',
    'bannerMid' => 'pour faire du monde un meilleur endroit!',
    'bannerBot' => 'Les gens ont besoin de votre aide! Alors pourquoi vous ne soyez pas celui qui dessine le sourire sur leurs visages.',

    'causesTitle' => 'Nos principaux objectifs',
    'cause1Title' => 'Aider les gens',
    'cause1Body' => 'Nous voulons réussir à aider les gens. Chaque service que nous offrons est en faveur de ceux qui en ont besoin.',
    'cause2Title' => 'Donnez de l\'inspiration',
    'cause2Body' => 'Nous voulons être un exemple pour d\'autres associations et communautés.',
    'cause3Title' => 'Aaider les personnes en cas de besoin',
    'cause3Body' => 'Il y a des gens qui ne peuvent même pas se permettre les nécessités de base, et nous décidons de les aider en collectant de l\'argent pour eux.',

    'noProfit' => "Nous sommes une équipe à but non lucratif<br>
                   qui travaille actuellement en <span style='color: red'>Tunisie</span>
                   avec des plans pour se développer davantage à l'avenir",
    'noProfitSub' => "Notre objectif ne sera <u>jamais</u> rentable.",
];
