<a class="btn btn-warning" href="{{ route('admin.users.generate_pwd_link', ['id' => $id, ]) }}" target="_blank">
    <i class="fas fa-key"></i>
</a>
