@extends('layouts.admin')

@section('header-links')

    <div class="row w-100 text-center mt-3 mt-lg-0 ml-lg-2 mx-auto px-0">
        <form action="{{ route('admin.users.search') }}" class="col-sm-12 col-md-9 container-fluid border row w-100 mx-auto py-2">
            <div class="col-sm-12 col-md-1 my-auto">
                <a href="{{ route('admin.users.index') }}"><i class="fas fa-backward fa-2x mt-2"></i></a>
            </div>

            <div class="col-sm-12 col-md-5 my-auto">
                <input type="search" name="query" class="form-control" value="{{ request('query') }}">
            </div>

            <div class="col-sm-12 col-md-4 my-3 my-md-0">
                <select class="custom-select" name="category">
                    <option value="name" {{ request('category') == 'name' ? 'selected' : '' }}>name</option>
                    <option value="email" {{ request('category') == 'email' ? 'selected' : '' }}>email</option>
                    <option value="id" {{ request('category') == 'id' ? 'selected' : '' }}>id</option>
                    <option value="admin" {{ request('category') == 'admin' ? 'selected' : '' }}>admin</option>
                    <option value="volunteer" {{ request('category') == 'volunteer' ? 'selected' : '' }}>volunteer</option>
                </select>
            </div>

            <div class="col-sm-12 col-md-2 my-auto">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
        </form>

        <!-- Button trigger modal -->
        <div class="col-sm-12 col-md-3 d-flex justify-content-center my-2 my-md-0">
            <button type="button" class="btn btn-primary my-auto" style="width: initial" data-toggle="modal" data-target="#CreateUserModal">
                Create User
            </button>
        </div>
    </div>

    @component('components.Admin.UserModal')
        @slot('mode')
            Create
        @endslot

        @slot('method')
            POST
        @endslot

        @slot('route')
            {{ route('admin.users.store') }}
        @endslot
    @endcomponent
@endsection

@section('panel')
    <div class="">

        @include('components.error-success')

        @component('components.Admin.UserModal')
            @slot('mode')
                Edit
            @endslot

            @slot('method')
                PATCH
            @endslot

            @slot('route')
                {{ route('admin.users.update', '') }}
            @endslot
        @endcomponent

        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr class="">
                    <th scope="col" style="width: 10%">#</th>
                    <th scope="col" style="width: 20%">Name</th>
                    <th scope="col" style="width: 20%">Email</th>
                    <th scope="col" style="width: 30%">Roles</th>
                    <th scope="col" style="width: 20%">Actions</th>
                </tr>
                </thead>
                <tbody>

                @foreach($users as $user)
                    @if(! ($user->hasGroup('superadmin') || $user->id == auth()->user()->id))

                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                <ul>
                                    @foreach($user->groups()->get() as $role)
                                        <li>{{ $role->name }}</li>
                                    @endforeach
                                </ul>
                            </td>
                            <td class="w-100" style="display: inline-flex">
                                @permission('edit-users-panel')
                                    <button type="button"
                                            class="btn btn-primary mr-2"
                                            data-toggle="modal"
                                            data-target="#EditUserModal"
                                            data-user_id="{{ $user->id }}"
                                            data-user_name="{{ $user->name }}"
                                            data-user_email="{{ $user->email }}"
                                            data-user_roles="@foreach($user->groups()->get() as $role){{ $role->slug."," }}@endforeach"
                                            data-user_permissions="@foreach($user->getAllPermissions() as $permission){{ $permission->slug."," }}@endforeach">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    @component('components.Admin.deleteBtn')
                                        @slot('title')
                                            Role
                                        @endslot

                                        @slot('route')
                                            {{ route('admin.users.destroy', $user->id)}}
                                        @endslot

                                        @slot('icon')
                                            <i class="fas fa-user-minus"></i>
                                        @endslot
                                    @endcomponent

                                    @component('components.Admin.generateLinkBtn')
                                        @slot('id')
                                            {{ $user->id }}
                                        @endslot
                                    @endcomponent
                                @endpermission
                            </td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
            <div class="text-xs-center">
                {{ $users->links() }}
            </div>

        </div>
    </div>
@endsection
