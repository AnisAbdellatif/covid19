@extends('errors.minimal')

@section('title', __('Not Found'))

@section('code', '404')

@section('message')
    @if(isset($message))
        {!! $message !!}

    @else
        {{ __('Not Found') }}
    @endif
@endsection
