<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/en');

Route::group(['prefix' => '{language}'], function () {
    Route::view('/', 'welcome')->name('welcome');

    Auth::routes();

    Route::namespace('Admin')
        ->prefix('admin')
        ->name('admin.')
        ->middleware(['auth', 'permissions:access-admin-page'])
        ->group(function () {
            Route::resource('dashboard', 'DashboardController');

            Route::middleware('permissions:access-auth-panel')
                ->group(function () {
                    Route::get('users/generate-pwd-link/{id}', 'UserController@generateLink')->name('users.generate_pwd_link');
                    Route::get('users/search', 'UserController@search')->name('users.search');
                    Route::resource('users', 'UserController');

                    Route::resource('roles', 'RoleController');

                    Route::resource('permissions', 'PermissionController');
                });

            Route::resource('requests', 'RequestController');

            Route::redirect('/', route('admin.dashboard.index', app()->getLocale()))->name("default");
        });

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/demands/taken', 'DemandController@taken')->name('demands.taken');

    Route::resource('demands', 'DemandController')
        ->middleware('auth');

    Route::namespace('Admin')
        ->middleware('auth')
        ->group(function () {
            Route::view('/change-password', 'auth.change_password')->name('change_password.edit');
            Route::patch('/change-password', 'UserController@updatePassword')->name('change_password.update');
        });
});
